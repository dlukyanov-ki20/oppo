<<<<<<< HEAD
﻿// Система моделирования
=======
// Система моделирования
>>>>>>> 1c627a18ce198c4f3e6b97f5ab05b39ff58b87fc
// Вариант 11. Интегрирование с использованием RKF45fix.
// Вывод результатов на консоль.
// Модель одна - двигатель постоянного тока с внутренним заданием параметров


/*

 В функции Print() добавил вывод в файл, предварительно объявив файл в main()
 объявляем массив в mainб предаём его в RKF45fix()  --> Fen() --> Model();

 Изменил функцию Model - Теперь значения подставляются из файла, а не прописаны в программе!

*/

/*    Закоментировал вызов mas в функциях !!!!!!!!!!*/
#include <conio.h>
#include <istream>
#include <string> 
#include <iostream>
#include <fstream> // для работы с потоками
#include <iomanip>	// Библиотека для форматирования вывода текста
#include<cstdio>
#include <windows.h> // Для вывода списка файлов(моделей)
#include <cstring>
#include <string>

using namespace std;

void Model(double t, double* dx, double* x, double* mass);
void Fen(int neq, double h, double t, double* S, double* x, double* dx, double* A1, double* A2, double* A3, double* A4, double* A5, double* mass);
void RKF45fix(int neq, double step, double start, double stop, double t, int Npoint, double* x, double* Result, double* mass);
void Print(double* Result, int N, int M, std::ostream& ost, ofstream& out);

double* load_model()
{
	cout << "Если вы хотите использовать файл, для загрузки модели - нажмите 1. Если хотите ввести данные вручную - нажмите 0\n";
	bool vruchnuyu_ili_fail;
	cin >> vruchnuyu_ili_fail;
	double* mass_ruch = new double[7];
	if (vruchnuyu_ili_fail == 0)
	{
		cout << "Введите параметры:\nU = ";
		cin >> mass_ruch[0];
		cout << "\nMs = ";
		cin >> mass_ruch[1];
		cout << "\nr = ";
		cin >> mass_ruch[2];
		cout << "\nL = ";
		cin >> mass_ruch[3];
		cout << "\nJ = ";
		cin >> mass_ruch[4];
		cout << "\nCe = ";
		cin >> mass_ruch[5];
		cout << "\nCm = ";
		cin >> mass_ruch[6];
	}
	else {
		cout << "Для просмотра файлов, имеющихся в базе - нажмите 1, иначе 0\n";
		bool regim;
		cin >> regim;

		wstring way_base;
		way_base = wstring(L"C:\\*");

		cout << "Если хотите загрузить модель извне - нажмите 0, иначе 1(Загрузится путь по умолчанию)\n";
		bool smena;
		cin >> smena;

		if (smena == 0) // Указываем путь к сторонней безе данных.
		{
			cout << "Введите путь к базе(после последнего сивола <обратный флеш> нужно указать символ *\n Пример : C:\Base\\* \n";
			cin.get();
			getline(wcin, way_base);
		}

		if (regim == 1)
		{
			WIN32_FIND_DATAW wfd;
			HANDLE const hFind = FindFirstFileW((L"\\\\?\\" + way_base).c_str(), &wfd); // Ниже расположенна функция для вывода списка файлов.
			if (INVALID_HANDLE_VALUE != hFind)
			{
				do
				{
					std::wcout << &wfd.cFileName[0] << std::endl;
				} while (NULL != FindNextFileW(hFind, &wfd));
				FindClose(hFind);
			}
		}

		string filename;
		cout << "Введите имя файла для открытия: (В будущем название двигателя)\n";
		cin >> filename;

		way_base.pop_back(); // Стираем символ "*" из пути к файлу - что бы на его место записать имя файла.

		ifstream ist(way_base);

		bool open = 0;


		do
		{
			string way_base_1(way_base.begin(), way_base.end());
			string way = way_base_1 + filename;
			ifstream ist(way);

			while (!ist.is_open())  //                                                                         !!!!!!   Обработчик ошибки - если "Имя введено неправильно"
			{
				cout << "Error - Файл не открылся, или нет такого файла\nВведите другое имя файла:\n";
				cin >> filename;
				string way = way_base_1 + filename;
				ifstream ist(way);                             // У нас эта штука объявляется в цикле -нужно её от туда вытащить. Здесь была проблема. Сейчас проблемы нет.
				if (ist.is_open())
				{
					open = 0;
					cout << "Файл открылся!\n";
					double n, m;
					ist >> n >> m;  // Считываем число строк файла ( позже это можно будет убрать, если число параметров будет фиксированно, если же оно будет различаться - данная строка поможет это реализовать).
					double* mass;
					mass = new double[n, m]; // Объявляем массив, куда записываются перменные из файла.
					int i = 0;
					for (int i = 0;i < n;++i)
					{
						for (int j = 0;j < m;++j)
						{
							ist >> mass[i, j];
						}
					}
					return mass;
				}
			}
			cout << "Файл открылся!\n";
			double n, m;
			ist >> n >> m;  // Считываем число строк файла ( позже это можно будет убрать, если число параметров будет фиксированно, если же оно будет различаться - данная строка поможет это реализовать).


			double** mass;
			mass = new double* [n]; // Объявляем массив, куда записываются перменные из файла.
			for (int i = 0;i < n;i++)
			{
				mass[i] = new double[m];
				for (int j = 0;j < m;j++)
				{
					ist >> mass[i][j];
				}
			}

			cout << "\nСчитался двухмерный массив\n";
			cout << "№          U       Ms          r       L        J           Ce        Cm\n";


			for (int i = 0;i < n;++i)
			{
				cout << i << " ";
				for (int j = 0;j < m;++j)
				{
					cout.width(10);
					cout << mass[i][j];
				}cout << "\n";
			}cout << "\n";

			//Здесь сделать выбор пользователем модели, и возвращать только одну модель.
			int №;
			cout << "Введите номер модели от 0 до " << n - 1 << "\n";
			cin >> №;

			double* itog_mass;
			itog_mass = new double[m];
			for (int i = 0;i < n;++i)
			{
				if (i == №)
				{
					for (int j = 0;j < m;++j)
					{
						itog_mass[j] = mass[i][j];
					}
				}
			}
			return itog_mass;


		} while (!open);
	}
	return mass_ruch;
}


int main()
{
	setlocale(LC_ALL, "rus");

	ofstream out("out.txt");

	double* mass;
	mass = load_model();  // Вызываем ф-ию загрузки модели.
	cout << "Считанные параметры модели:\n";
	cout << "U = " << mass[0] << "\nMs = " << mass[1] << "\nr = " << mass[2] << "\nL = " << mass[3] << "\nJ = " << mass[4] << "\nCe = " << mass[5] << "\nCm = " << mass[6] << "\n";



	const int neq = 3;						// Число переменных состояния
	int Npoint;								// Число точек вывода
	int i;									// Индексы в циклах
	double t;								// Время текущее

	double* Result, * x;

	x = new double[neq];					// Массив значений переменных состояния

	for (i = 0; i < neq; i++)
	{
		x[i] = 0.;							// Начальные значения переменных состояния
	}

	double start = 0.;						// Время начала интегрирования
	double stop = 0.1;						// Время окончания интегрирования
	double step = 0.00001;					// Шаг интегрирования по времени
	Npoint = 100;							// Число точек вывода

	Result = new double[(neq + 1) * (Npoint + 1)];			// Массив для вывода результатов расчётов

	t = start;												// Начальное время интегрирования
	RKF45fix(neq, step, start, stop, t, Npoint, x, Result, mass);	// Численное интегрирование

	Print(Result, (Npoint + 1), (neq + 1), cout, out);			// Вывод результатов расчёта на экран

	delete[] Result;
	delete[] x;


	return 0;
}



// Модель двигателя постоянного тока
// Тип функции "void", так как вывод данных происходит через параметры (в скобках)
// Массивы передаются через указатели (*)
//
// t - текущее время
// x - входной массив переменных состояния (вводится в функцию)
// dx - выходной массив производных переменных состояния (выводится из функции)
void Model(double t, double* dx, double* x, double* mass)
{

	//________________________________________________________________________________________Подставляем значения из считанного файла.

		// внешние воздействия
	double U = mass[0];				// Напряжение на якорной обмотке
	double Ms = mass[1];		// Момент сопротивления на валу двигателя
// Параметры двигателя постоянного тока
	double r = mass[2];				// Активное сопротивление якорной обмотки
	double L = mass[3];			// Индуктивность обмотки
	double J = mass[4];			// Момент инерции
	double Ce = mass[5];			// Конструкционный коэффициент
	double Cm = mass[6];			// Конструкционный коэффициент


	// Система дифференциальных уравнений двигателя постоянного тока
	dx[0] = (-r * x[0] - Ce * x[1] + U) / L;	// x[0] - ток якорной обмотки
	dx[1] = (Cm * x[0] + Ms) / J;				// x[1] - угловая скорость вала двигателя
	dx[2] = x[1];								// x[2] - угол поворота вала двигателя
}



// Метод интегрирования Рунге-Кутты-Фельберга
void Fen(int neq, double h, double t, double* S, double* x, double* dx, double* A1, double* A2, double* A3, double* A4, double* A5, double* mass)
{
	int k;										// Индекс для циклов
	double ch;									// Вспомогательный шаг интегрирования (как доля основного шага)

	Model(t, dx, x, mass);

	ch = h / 4.;
	for (k = 0; k < neq; k++)
	{
		A5[k] = x[k] + ch * dx[k];
	}
	Model(t + ch, dx, A5, mass);
	for (k = 0; k < neq; k++)
	{
		A1[k] = dx[k];
	}


	ch = (3. / 32.) * h;
	for (k = 0; k < neq; k++)
	{
		A5[k] = x[k] + ch * (dx[k] + 3. * A1[k]);
	}
	Model(t + (3. / 8.) * h, dx, A5, mass);
	for (k = 0; k < neq; k++)
	{
		A2[k] = dx[k];
	}

	ch = h / 2197.;
	for (k = 0; k < neq; k++)
	{
		A5[k] = x[k] + ch * (1932. * dx[k] + 7296. * A2[k] - 7200. * A1[k]);
	}
	Model(t + (12. / 13.) * h, dx, A5, mass);
	for (k = 0; k < neq; k++)
	{
		A3[k] = dx[k];
	}

	ch = h / 4104.;
	for (k = 0; k < neq; k++)
	{
		A5[k] = x[k] + ch * (8341. * dx[k] - 845. * A3[k] + 29440. * A2[k] - 32832. * A1[k]);
	}
	Model(t + h, dx, A5, mass);
	for (k = 0; k < neq; k++)
	{
		A4[k] = dx[k];
	}

	ch = h / 20520.;
	for (k = 0; k < neq; k++)
	{
		A1[k] = x[k] + ch * (-6080. * dx[k] - 5643. * A4[k] + 9295. * A3[k] - 28352. * A2[k] + 41040. * A1[k]);
	}
	Model(t + h / 2., dx, A1, mass);
	for (k = 0; k < neq; k++)
	{
		A5[k] = dx[k];
	}

	ch = h / 7618050.;					// Шаг 6. Окончательный расчёт переменных состояния
	for (k = 0; k < neq; k++)
	{
		S[k] = x[k] + ch * (902880. * dx[k] + 277020. * A5[k] - 1371249. * A4[k] + 3855735. * A3[k] + 3953664. * A2[k]);
	}
}



// Вызывающая программа для метода Рунге-Кутты-Фельберга
// Обеспечивает расчёты до заданного конечного времени интегрирования
// Вызывает функцию Fen() для интегрирования на шаге
//
// neq - число переменных состояния
// step - шаг по времени для вывода результатов
// start - время начала интегрирования
// stop - время окончания интегрирования
// t - текущее время
// x - массив значений переменных состояния
// Result - массив результатов расчётов
void RKF45fix(int neq, double step, double start, double stop, double t, int Npoint, double* x, double* Result, double* mass)
{
	int j, k;									// Индексы для циклов
	double h;									// Шаг по времени для интегирования
	double hprint;								// Шаг по времени для вывода
	double tprint;								// Временя вывода очередной точки
	double tt, dt;								// Вспомогательные переменные при выборе значения текущего времени
	double* s;									// Массив значений переменных состояния в конце очередного шага интегрирования
	double* dx;									// Массив значений производных переменных состояния
	double* A1, * A2, * A3, * A4, * A5;				// Вспомогательные динамические массивы с размерностью по числу переменных состояния
	A1 = new double[neq];
	A2 = new double[neq];
	A3 = new double[neq];
	A4 = new double[neq];
	A5 = new double[neq];
	s = new double[neq];
	dx = new double[neq];

	tprint = start;								// Первое значение времени - start
	Result[0] = start;							// Запись в первую строку в первую позицию значения начального времени интегрирования
	for (j = 0; j < neq; j++)
	{
		Result[1 + j] = x[j];					// Запись в первую строку начальных значений переменых состояния
	}
	hprint = (stop - start) / Npoint;			// Шаг вывода результатов
	if (hprint < step) h = hprint;				// Шаг интегрирования - не более шага вывода результатов
	if (hprint >= step) h = step;				// 
	t = start;

	k = 0;										// Подсчёт числа выводимых точек
	while (t <= stop)
	{
		tprint = tprint + hprint;				// Установление нового времени вывода tprint с заданным шагом hprint
		while (t < tprint)
		{
			dt = tprint - t;					// Провряется, приходится ли текущее время на окончание времени вывода
			if (h < dt) tt = h;
			if (h >= dt) tt = dt;
			Fen(neq, tt, t, s, x, dx, A1, A2, A3, A4, A5, mass);
			for (j = 0; j < neq; j++)
			{
				x[j] = s[j];	// Новые значения переменных состояния s записываются в массив x для следующего шага
			}
			t = t + tt;
		}
		k++;										// Следующая строка в массиве результатов
		Result[k * (neq + 1)] = t;					// Запись в выходной массив результатов для очередной точки: время
		for (j = 0; j < neq; j++)
		{
			Result[k * (neq + 1) + 1 + j] = x[j];	// Запись в выходной массив результатов для очередной точки: переменные состояния
		}
	}

	// Удаление из памяти динамических массивов
	delete[] A1;
	delete[] A2;
	delete[] A3;
	delete[] A4;
	delete[] A5;
	delete[] s;
}



// Вывод данных в виде форматированной таблицы на экран или в файл
//
// Result - выводимый массив, передаваемый по указателю
// N - число строк
// M - число столбцов
// ost - устройство вывода (например, cout - на экран)
void Print(double* Result, int N, int M, std::ostream& ost, ofstream& out)
{
	int i, j;
	ost << setw(15) << "Время" << setw(15) << "Ток" << setw(15) << "Скорость" << setw(15) << "Угол" << endl;
	//off << setw(15) << "Время" << setw(15) << "Ток" << setw(15) << "Скорость" << setw(15) << "Угол" << endl;
	ost.setf(ios::right);					// Форматирование: выравнивать по правому краю
	ost.setf(ios::fixed);					// Форматирование: выводить числа с точкой
	ost.setf(ios::showpoint);				// Форматирование: выводить нули в конце числа

	for (i = 0; i < N * M; i = i + M)			// Одномерный массив выводится как двумерный (M - число позиций в строке)
	{
		for (j = 0; j < M; j++)
		{
			ost << setw(15) << Result[i + j];
			out << setw(15) << Result[i + j];// setw(15): ширина поля для вывода - 15 знаков
		}
		ost << endl;
		out << endl;
	}
}